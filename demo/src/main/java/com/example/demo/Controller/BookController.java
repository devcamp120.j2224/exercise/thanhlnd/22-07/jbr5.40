package com.example.demo.Controller;

import java.util.ArrayList;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Book;
import com.example.demo.Service.BookService;

@RestController
public class BookController {

    @GetMapping("/book-quantity")
    public ArrayList<Book> getBookFromQuantity(@RequestParam(name = "qty")int qty){
        ArrayList<Book> listBook = BookService.getAllBookList();

        ArrayList<Book> findBook = new ArrayList<>();
        
        for(Book book : listBook){
            if(book.getQty() >= qty)
            findBook.add(book);
        }
         return findBook;
    }

    
    @GetMapping("/books")
    public ArrayList<Book> getCountry(){
        ArrayList<Book> books = BookService.getAllBookList();
        return books;
    }
}
